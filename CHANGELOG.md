# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.0

**Features**
Initial Version

**Bugfixes**
N/A

**Known Issues**
Currently only supports Cloudflare for DNS verification.

