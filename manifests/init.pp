# Install lego https://github.com/xenolf/lego
#
# @summary install lego and setup common environment
#
# @example
#   include lego
class lego (
  String $cron_scripts_path = $lego::params::cron_scripts_path,
) inherits lego::params {
  file { '/usr/local/bin/lego' :
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/lego/usr/local/bin/lego',
  }

  file { $lego::cron_scripts_path:
    ensure => directory,
    purge  => true,
  }

}
