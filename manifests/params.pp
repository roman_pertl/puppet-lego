# This class contains all the default parameter values for this module.
#
# @summary all default parameter values
#
# @example
#   include lego::params
class lego::params {
  $acme_server = 'https://acme-v02.api.letsencrypt.org/directory' # production letsencrypt server
  $cron_scripts_path = "${::puppet_vardir}/lego" # path for renewal scripts called by cron
}
