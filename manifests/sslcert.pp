# Install a SSL Certificate from letsencrypt with lego
#
# @summary install a ssl certificate with lego
#
# @example
#   lego::sslcert { 'test.rpertl.io':
#       domainname  => 'roman@pertl.org',
#       email       => 'roman@pertl.org',
#       dnsprovider => 'cloudflare',
#       cloudflare_email => 'roman@pertl.org',
#       cloudflare_api_key => 'jairiez9yai1Gog6',
#   }
##   lego::sslcert { 'namevar': }
define lego::sslcert(
  String $domainname,
  String $email,
  String $dnsprovider = 'cloudflare',
  Optional[String] $cloudflare_email,
  Optional[String] $cloudflare_api_key,
  Integer $renewal_days = 30,
  String $acme_server = $::lego::acme_server,
) {
  $sanitized_name = regsubst($name, '([^a-z0-9])+', '_', 'IG')
  $sanitized_domainname = regsubst($domainname, '^\*', '_', 'IG')

  if ($dnsprovider == 'cloudflare') {
    # FIXME check if cloudflare_email and cloudflare_api_key are set
      $dnscredentials_cron = "CLOUDFLARE_EMAIL=${cloudflare_email} CLOUDFLARE_API_KEY=${cloudflare_api_key}"
      $dnscredentials_exec = [ "CLOUDFLARE_EMAIL=${cloudflare_email}", "CLOUDFLARE_API_KEY=${cloudflare_api_key}" ]
  }
  else {
      fail('not yet implemented')
  }

  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    "${::lego::cron_scripts_path}/renew-${sanitized_name}.sh":
      content => template('lego/renew-script.sh.erb'),
  }

  $cron_hour = fqdn_rand(24, $sanitized_name) # 0 - 23, seed is sanitized_name plus fqdn
  $cron_minute = fqdn_rand(60, $sanitized_name) # 0 - 59, seed is sanitized_name plus fqdn

  cron { "renew-sslcert-${sanitized_name}":
      command => "${::lego::cron_scripts_path}/renew-${sanitized_name}.sh",
      user    => 'root',
      hour    => $cron_hour,
      minute  => $cron_minute,
  }

  exec { "initial-ssl-cert-${sanitized_name}":
    environment => $dnscredentials_exec,
    user        => 'root',
    creates     => "/etc/lego/certificates/${sanitized_domainname}.pem",
    command     => "/usr/local/bin/lego --domains ${domainname} --server ${acme_server} --email ${email} --dns ${dnsprovider} -pem -a --path /etc/lego run", # lint:ignore:140chars
    #    command     => @("EOT"/)
    #      /usr/local/bin/lego --domains ${domainname} \
    #      --server ${acme_server} \
    #      --email ${email} --dns ${dnsprovider} --pem -a --path /etc/lego run",
    #      | EOT
  }

}
