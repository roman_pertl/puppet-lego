require 'spec_helper'

describe 'lego::sslcert' do
  let(:pre_condition) { 'class { lego: }' }
  let(:title) { 'foo.example.org' }
  let(:params) do
    {
      domainname: 'foo.example.org',
      email: 'foo@example.org',
      dnsprovider: 'cloudflare',
      cloudflare_email: 'foo@example.org',
      cloudflare_api_key: 'neeBai3nei9EiYoo5vef',
      renewal_days: 30,
      acme_server: 'https://acme-staging-v02.api.letsencrypt.org/directory',
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with a single domain' do
        # let(:title) { 'foo.example.org' }

        it {
          is_expected.to compile
          is_expected.to contain_exec('initial-ssl-cert-foo_example_org') \
            .with_command('/usr/local/bin/lego --domains foo.example.org --server https://acme-staging-v02.api.letsencrypt.org/directory --email foo@example.org --dns cloudflare -pem -a --path /etc/lego run') # rubocop:disable Metrics/LineLength
          is_expected.to contain_cron('renew-sslcert-foo_example_org') \
            .with_command('/tmp/lego/renew-foo_example_org.sh')
          is_expected.to contain_file('/tmp/lego/renew-foo_example_org.sh') \
            .with_content(%r{CLOUDFLARE_EMAIL=foo@example.org CLOUDFLARE_API_KEY=neeBai3nei9EiYoo5vef /usr/local/bin/lego --domains foo.example.org --server https://acme-staging-v02.api.letsencrypt.org/directory --email foo@example.org --dns cloudflare -pem -a --path /etc/lego renew --days 30}) # rubocop:disable Metrics/LineLength
          is_expected.to contain_exec('initial-ssl-cert-foo_example_org')\
            .with_environment(['CLOUDFLARE_EMAIL=foo@example.org', 'CLOUDFLARE_API_KEY=neeBai3nei9EiYoo5vef'])
        }
      end
    end
  end
end
